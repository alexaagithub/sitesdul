## README
![SDUL LOGO](https://gitlab.com/alexaagithub/sitesdul/-/raw/master/sdul/static/images/redname.png)

Website da Sociedade de Debate da Universidade de Lisboa. Criado com [Hugo](https://gohugo.io/) a partir do tema [Bigspring](https://github.com/themefisher/bigspring-light).

## Instalação
```bash

git clone git@gitlab.com:alexaagithub/sitesdul.git

$ cd sdul

$ hugo server -D
```