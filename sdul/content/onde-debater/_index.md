---
title: "Onde debater"
subtitle: ""
# meta description
description: "Onde debater?"
draft: false
---

* **Online:** Todas as quintas-feiras às 18h30 através do Zoom.

{{< cta-button >}}
<br>


* **Presencialmente:** A divulgar em breve

 
{{< cta2-button "Inscrição nos debates presenciais" >}}

