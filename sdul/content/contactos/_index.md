---
title: "Contactos"
subtitle: ""
# meta description
description: "Contactos da SDUL"
draft: false
---


#### Núcleo Diretivo 
* **Presidente:** Francisca Baptista
* **Vice-Presidente:** Inês Raposo
* **Secretária-Geral:** Alexandra Pardal
* **Tesoureira:** Leonor Marques

Contacto: sdul.geral@gmail.com

#### Organização dos Debates Semanais 📝
* Salomé Rosa
* Ivan Mira
* Ianira Vieira
* Alexandre Dias

Contacto: sdul.dep.gestao@gmail.com

#### Eventos 🗓
**Coordenadora:** Matilde Almeida e Silva
* José Correia
* Cláudia Pacheco

Contacto: sdul.eventos@gmail.com

#### Marketing 📣
**Coordenadora:** Lara Francisco
* David Greer
* Maria Luiza Azevedo
* João Azevedo


Contacto: marketingsdul@gmail.com





 




 



